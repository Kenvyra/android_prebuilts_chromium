#!/usr/bin/bash
CWD=$(dirname $0)
cd $CWD/x86
rm *.apk
wget https://gitlab.com/CalyxOS/platform_prebuilts_calyx_chromium_x86/-/raw/android13/TrichromeLibrary.apk
wget https://gitlab.com/CalyxOS/platform_prebuilts_calyx_chromium_x86/-/raw/android13/TrichromeWebView.apk
cd ../x64
rm *.apk
wget https://gitlab.com/CalyxOS/platform_prebuilts_calyx_chromium_x64/-/raw/android13/TrichromeLibrary6432.apk
wget https://gitlab.com/CalyxOS/platform_prebuilts_calyx_chromium_x64/-/raw/android13/TrichromeWebView6432.apk
cd ../arm64
rm *.apk
wget https://gitlab.com/CalyxOS/platform_prebuilts_calyx_chromium_arm64/-/raw/android13/TrichromeLibrary6432.apk
wget https://gitlab.com/CalyxOS/platform_prebuilts_calyx_chromium_arm64/-/raw/android13/TrichromeWebView6432.apk
